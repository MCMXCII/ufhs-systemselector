<?php

class Ufhs_Systemselector_Model_Resource_Result_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('systemselector/result');
    }

}