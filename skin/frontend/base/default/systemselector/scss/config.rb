# note: this should never truly be refernced since we are using relative assets
http_path = "/skin/frontend/base/default/systemselector/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../img"
generated_images_dir = "../img"
javascripts_dir = "../js"

relative_assets = true

output_style = (environment == :development) ? :expanded : :compressed
line_comments = (environment == :development) ? true : false

sass_options = {:sourcemap => true}

# compass watch -e development