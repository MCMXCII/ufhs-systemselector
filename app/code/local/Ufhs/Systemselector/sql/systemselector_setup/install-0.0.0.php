<?php

$installer = $this;

$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('systemselector/product')};

	CREATE TABLE {$installer->getTable('systemselector/product')} (
	`id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL,
	`description` VARCHAR(1023) NOT NULL,
	`mage_id` INT(15) NOT NULL,
	`priority` INT(15) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;

	INSERT INTO {$installer->getTable('systemselector/product')} (`id`, `title`, `description`, `mage_id`, `priority`)
	VALUES
		(1, 'ProWarm Electric Mat Systems', '<p>The ProWarm&trade; underfloor heating mats are quick and easy to install, whatever shape room you have. Made from super-thin fiberglass mesh, our mats are available in a range of sizes to suit your installation and come in 100w, 150w or 200w per m2 outputs.  They can be used under a variety of floor finishes, from tile, stone and wood to carpet and vinyl making them the ideal choice for most retro fit projects</p>', 10959, 1),
		(2, 'ProWarm Electric Cables Systems', '<p>Designed to be installed under a variety of floor finishes, including tile and stone floors, plus wood, carpet and vinyl, our range of underfloor heating cables offer a flexible and simple solution for installations.  The advantage of a loose cable system means that the fixtures can be installed around awkward static items, such as sinks, toilets and kitchen appliances.</p>', 11084, 1),
		(3, 'ProWarm Electric Inscreed Heating Systems', '<p>The Inscreed cable systems are only normally used in new extensions or conservatory project as they take longer to heat up and produce a higher output of 17 watts per linear metre.</p><p>In screed cables are designed to be fixed to re-bar, which has been laid into foil faced insulation (kingspan/celotex) and directly under a minimum 50mm screed.</p>', 11085, 1),
		(4, 'ProWarm Electric Foil Mat Systems', '<p>The ProWarm&trade; underwood heating mats are easy to use and can be installed under almost any wooden flooring, including laminate. This product?s versatility sets it apart from our competitors, installation requires no adhesives or levelling compound</p>', 11121, 1),
		(5, 'ProWarm Water Standard Output Systems', '<p>ProWarm&trade; Water Underfloor Heating Systems are the ideal choice for homeowners and tradesmen wishing to install a Premium Branded water underfloor heating kit.</p><p>ProWarm&trade; Water Underfloor Heating Single room kits are ideal for combining one area of water underfloor heating (eg. living room) with a standard radiator system</p>', 11030, 1),
		(6, 'ProWarm Water High Output Systems', '<p>ProWarm&trade; Water Underfloor Heating Systems are the ideal choice for homeowners and tradesmen wishing to install a Premium Branded water underfloor heating kit.</p><p>A high output system is suitable for areas of high heat loss, ideal for conservatories, extensions and external buildings with lots of glass.</p><p>ProWarm&trade; High Output - Water Underfloor Heating Single room kits are ideal for combining one area of water underfloor heating in a high heat loss area (eg. conservatory) with a standa', 11031, 1),
		(7, 'ProWarm Water Low Profile Systems', '<p>ProWarm&trade; Water Underfloor Heating Systems are the ideal choice for homeowners and tradesmen wishing to install a Premium Branded water underfloor heating kit.</p><p>A low profle system uses a smaller pipe diameter which attains a lower floor height. These systems can be used on existing floors with minimal disruption.</p>', 11033, 1);
	");

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('systemselector/result')};

	CREATE TABLE {$installer->getTable('systemselector/result')} (
	`id` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
	`result` INT(7) NOT NULL,
	`product` INT(15) NOT NULL,
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;

	INSERT INTO {$installer->getTable('systemselector/result')} (`id`, `result`, `product`)
	VALUES
		(1, 1111, 3),
		(2, 1112, 3),
		(3, 1113, 3),
		(4, 1121, 6),
		(5, 1122, 6),
		(6, 1123, 6),
		(7, 1211, 2),
		(8, 1212, 4),
		(9, 1213, 2),
		(10, 1221, 2),
		(11, 1222, 4),
		(12, 1223, 2),
		(13, 1111, 6),
		(14, 1112, 6),
		(15, 1113, 6),
		(16, 1121, 2),
		(17, 1122, 4),
		(18, 1123, 2),
		(19, 1111, 2),
		(20, 1112, 4),
		(21, 1113, 2),
		(22, 2111, 3),
		(23, 2112, 3),
		(24, 2113, 3),
		(25, 2121, 6),
		(26, 2122, 6),
		(27, 2123, 6),
		(28, 2211, 7),
		(29, 2212, 7),
		(30, 2213, 7),
		(31, 2221, 7),
		(32, 2222, 7),
		(33, 2223, 7),
		(34, 5111, 3),
		(35, 5112, 3),
		(36, 5113, 3),
		(37, 5121, 6),
		(38, 5122, 6),
		(39, 5123, 6),
		(40, 5211, 7),
		(41, 5212, 7),
		(42, 5213, 7),
		(43, 5221, 7),
		(44, 5222, 7),
		(45, 5223, 7),
		(46, 2111, 6),
		(47, 2112, 6),
		(48, 2113, 6),
		(49, 2121, 1),
		(50, 2122, 4),
		(51, 2123, 4),
		(52, 2211, 1),
		(53, 2212, 4),
		(54, 2213, 4),
		(55, 2221, 1),
		(56, 2222, 4),
		(57, 2223, 1),
		(58, 5111, 6),
		(59, 5112, 6),
		(60, 5113, 6),
		(61, 5121, 1),
		(62, 5122, 4),
		(63, 5123, 4),
		(64, 5211, 1),
		(65, 5212, 4),
		(66, 5213, 4),
		(67, 5221, 1),
		(68, 5222, 4),
		(69, 5223, 1),
		(70, 3111, 3),
		(71, 3112, 3),
		(72, 3113, 3),
		(73, 3121, 6),
		(74, 3122, 6),
		(75, 3123, 6),
		(76, 3211, 7),
		(77, 3212, 7),
		(78, 3213, 7),
		(79, 3221, 7),
		(80, 3222, 7),
		(81, 3223, 7),
		(82, 4111, 3),
		(83, 4112, 3),
		(84, 4113, 3),
		(85, 4121, 6),
		(86, 4122, 6),
		(87, 4123, 6),
		(88, 4211, 7),
		(89, 4212, 7),
		(90, 4213, 7),
		(91, 4221, 7),
		(92, 4222, 7),
		(93, 4223, 7),
		(94, 3111, 5),
		(95, 3112, 5),
		(96, 3113, 5),
		(97, 3121, 1),
		(98, 3122, 4),
		(99, 3123, 4),
		(100, 3211, 1),
		(101, 3212, 4),
		(102, 3213, 4),
		(103, 3221, 1),
		(104, 3222, 4),
		(105, 3223, 4),
		(106, 4111, 5),
		(107, 4112, 5),
		(108, 4113, 5),
		(109, 4121, 1),
		(110, 4122, 4),
		(111, 4123, 4),
		(112, 4211, 1),
		(113, 4212, 4),
		(114, 4213, 4),
		(115, 4221, 1),
		(116, 4222, 4),
		(117, 4223, 4);
	");

$installer->endSetup();